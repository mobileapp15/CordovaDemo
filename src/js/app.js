/*globals ko */

(function () {
    "use strict";
    
    var storageService = {
        load: function () {
            return JSON.parse(localStorage.getItem('todos') || '[{"text": "Hello"}]');
        },
        add: function (todo) {
            var todos = this.load();
            todos.unshift(todo);
            localStorage.setItem('todos', JSON.stringify(todos));
        },
        save: function (todos) {
            localStorage.setItem('todos', JSON.stringify(todos));
        }
    };
    
    function ToDoListViewModel() {
        var self = this;
        
        self.todos = ko.observableArray(storageService.load());
        
        self.removeToDo = function () {
            var todo = this;
            navigator.notification.confirm('Are you sure you want to delete this todo?', function (button) {
                if (button === 1) {
                    self.todos.remove(todo);
                    storageService.save(self.todos());
                }
            }, 'Delete ToDo', ['Yes', 'No']);
            return false;
        };
    }
    
    function AddToDoViewModel() {
        var self = this;
        
        self.text = ko.observable('');
        
        self.addToDo = function () {
            storageService.add({
                text: self.text()
            });
            window.history.back();
        };
    }
    
    ko.components.register('todo-list', {
        viewModel: ToDoListViewModel,
        template: { element: 'todo-list' }
    });
    
    ko.components.register('add-todo', {
        viewModel: AddToDoViewModel,
        template: { element: 'add-todo' }
    });
    
    ko.router.route('/', 'todo-list');
    ko.router.route('/new', 'add-todo');
    ko.router.start({hashbang: true, basePath: window.location.pathname});
    ko.applyBindings();
}());